import axios from 'axios';
import {keys} from 'lodash/fp';

export interface Poke {
    id: number;
    image: string;
    pokeIndex: number;
    sprites: { [key: string]: string };
    images: string[];
}

export class PokemonService {
    private baseUrl = 'https://pokeapi.co/api/v2';
    private imageUrl = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

    async getPokemon(offset = 0) {
        return await axios.get(`${this.baseUrl}/pokemon?offset=${offset}&limit=25`).then(({data}) => {
            const {results} = data;

            return results.map((poke: Poke, index: number) => {
                poke.image = this.getPokeImage(offset + index + 1);
                poke.pokeIndex = offset + index + 1;

                return poke;
            });
        });
    }

    async findPokemon(search: string) {
        return await axios.get(`${this.baseUrl}/pokemon/${search}`).then(({data}) => {
            data.image = this.getPokeImage(data.id);
            data.pokeIndex = data.id;

            return data;
        });
    }

    getPokeImage(index: number) {
        return `${this.imageUrl}${index}.png`;
    }

    async getPokeDetails(index: string) {
        return await axios.get(`${this.baseUrl}/pokemon/${index}`).then(({data}) => {
            const sprites = keys(data.sprites);

            data.images = sprites.map((spriteKey: string) => {
                return typeof data.sprites[spriteKey] === 'string' ? data.sprites[spriteKey] : null;
            }).filter(img => img);

            return data;
        });
    }
}
